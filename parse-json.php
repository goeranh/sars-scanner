#! /usr/bin/php
<?php
include 'config.php';

$sql = "SELECT scans.id, content FROM scans inner join sites on sites.id=scans.site where sites.type='json' and parsed=0";
$sql = "SELECT scans.id, content FROM scans where site=7 and parsed=0";
foreach ($pdo->query($sql) as $row){
    $fromJSON = json_decode($row['content']);
    foreach ($fromJSON->features as $feature){
        $feature = $feature->attributes;
        $place = $feature->Country_Region . '.' . $feature->Province_State;
        $amount = $feature->Confirmed;
        $cured = $feature->Recovered;
        $deaths = $feature->Deaths;
        $active = $amount-$deaths-$cured;

        $sql = "INSERT INTO `data`(`scan`, `place`, `amount`, `deaths`, `active`, `cured`) VALUES (?,?,?,?,?,?)";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1, $row['id']);
        $stmt->bindParam(2, $place);
        $stmt->bindParam(3, $amount);
        $stmt->bindParam(4, $deaths);
        $stmt->bindParam(5, $active);
        $stmt->bindParam(6, $cured);
        if (!$stmt->execute()){
            var_dump($stmt->errorInfo());
        }
    }

    $sql = "UPDATE `scans` SET `parsed`=1 WHERE ?";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(1, $row['id']);
    if (!$stmt->execute()){
        var_dump($stmt->errorInfo());
    }
}