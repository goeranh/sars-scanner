<?php
include "config.php";

$sql = "SELECT DISTINCT place FROM data";
foreach ($pdo->query($sql) as $place){
    $sql = "SELECT data.id, time, amount FROM data inner join scans on scans.id=data.scan where place = ? order by time asc ";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(1, $place['place']);
    $stmt->execute();
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    //var_dump($data[1]['id']);

    $oneDaySteps = array();

    for ($i = 0; $i < sizeof($data); $i++){
        //var_dump($i%144);
        if (($i+1)%144 == 0){
            //var_dump($data[$i]);
            $oneDaySteps[] = intval($data[$i]['amount']);
        }
    }

    $ratios = array();

    for ($i = 1; $i < sizeof($oneDaySteps); $i++){
        $ratios[] = $oneDaySteps[$i]/$oneDaySteps[$i-1];
    }

    //var_dump($ratios);

    $sum = 0;
    foreach ($ratios as $ratio){
        $sum = $sum + $ratio;
    }

    if (count($ratios)>0) {
        echo $place['place'].': '.$sum/count($ratios)."\n";
    }
}