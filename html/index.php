<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="refresh" content="60">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <title>Info an Coronavirus</title>
</head>
<body>
<?php
include '../config.php';

//in total
/*$sql = "SELECT sum(amount) as total, sum(active) as active, sum(cured) as cured, sum(deaths) as dead FROM data where scan = (select MAX(scans.id) from scans inner join data on data.scan=scans.id where site=2) order by active desc ";
$data = $pdo->query($sql)->fetchAll(PDO::FETCH_ASSOC)[0];
echo '<h4>Insgesammt hat das Coronavirus '.$data['total'].' Menschen infiziert. Von dieser Zahl sind noch '.$data['active'].' Fälle offen. Von den restlichen sind '.$data['cured'].' geheilt und '.$data['dead'].' gestorben. Das entspricht einer theorethischen leathalität von <b>'.number_format(intval($data['dead'])/intval($data['cured'])*100, 4).'%</b>, wenn man die Dunkelziffer der Infektionen ausser acht lässt.</h4>';
*/

//grafik
$sql = "SELECT scans.id, place, amount, time FROM `data` inner join scans on scans.id=data.scan WHERE place like 'Germany.' group by amount order by time asc ";
$stmt = $pdo->query($sql);
$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>

<div class="container"  style="height: 1000px; width: 100%; position: relative">
    <canvas id="germany-chart" style="height: 100%; width: 100%"></canvas>
</div>

<script>
    let myChart = document.getElementById('germany-chart').getContext('2d');

    // Global Options
    Chart.defaults.global.defaultFontFamily = 'Lato';
    Chart.defaults.global.defaultFontSize = 18;
    Chart.defaults.global.defaultFontColor = '#777';

    let massPopChart = new Chart(myChart, {
        type:'bar', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
        data:{
            labels:[
                <?php
                for ($i = 0 ; $i < count($data); $i++){
                    if ($i == 0){
                        echo "'".$data[$i]['time']."'";
                    }else{
                        echo ', \''.$data[$i]['time']."'";
                    }
                }
                ?>
            ],
            datasets:[{
                label:'Anzahl',
                data:[
                    <?php
                    for ($i = 0 ; $i < count($data); $i++){
                        if ($i == 0){
                            echo "".$data[$i]['amount']."";
                        }else{
                            echo ', '.$data[$i]['amount']."";
                        }
                    }
                    ?>
                ],
                //backgroundColor:'green',
                backgroundColor:[
                    /*'rgba(255, 99, 132, 0.6)',
                    'rgba(54, 162, 235, 0.6)',
                    'rgba(255, 206, 86, 0.6)',
                    'rgba(75, 192, 192, 0.6)',
                    'rgba(153, 102, 255, 0.6)',
                    'rgba(255, 159, 64, 0.6)',
                    'rba(255, 99, 132, 0.6)'*/
                ],
                borderWidth:1,
                borderColor:'#777',
                hoverBorderWidth:3,
                hoverBorderColor:'#000'
            }]
        },
        options:{
            responsive: false,
            maintainAspectRatio: false,
            title:{
                display:true,
                text:'Anzahl der Infektionsfälle in Deutschland',
                fontSize:25
            },
            legend:{
                display:true,
                position:'right',
                labels:{
                    fontColor:'#000'
                }
            },
            layout:{
                padding:{
                    left:0,
                    right:0,
                    bottom:0,
                    top:0
                },
            },
            tooltips:{
                enabled:true
            }
        }
    });
</script>

<?php

/*
//Tabelle
$sql = "SELECT * FROM data where scan = (select MAX(scans.id) from scans inner join data on data.scan=scans.id where site=4) and place like 'Deutschland.%' order by amount desc";
echo '<h2>Fallzahlen in Deutschland - Nach Robert Koch Institut</h2>';
echo '<table style="width: 50%">';
echo '<tr><th>Platz</th><th>Land</th><th>kumulative Fallzahl</th></tr>';
$i = 1;
foreach ($pdo->query($sql) as $row){
    echo '<tr><td>'.$i.'</td><td><a href="laender.php?land='.trim($row['place']).'">'.str_replace("D-", "", $row['place']).'</a></td><td>'.$row['amount'].'</td></tr>';
    $i++;
}
echo '</table>';


$sql = "SELECT * FROM data where scan = (select MAX(scans.id) from scans inner join data on data.scan=scans.id where site=7) order by active desc limit 500";
echo '<h2>Weltweite Daten - Top 50</h2>';
echo '<table style="width: 100%">';
echo '<tr><th>Platz</th><th>Land</th><th>Aktive Fälle</th><th>kumulative Fallzahl</th><th>geheilte</th><th>Todesfälle</th></tr>';
$i = 1;
foreach ($pdo->query($sql) as $row){
    echo '<tr><td>'.$i.'</td><td><a href="laender.php?land='.trim($row['place']).'">'.$row['place'].'</a></td><td>'.$row['active'].'</td><td>'.$row['amount'].'</td><td>'.$row['cured'].'</td><td>'.$row['deaths'].'</td></tr>';
    $i++;
}
echo '</table>';*/
?>
</body>
</html>