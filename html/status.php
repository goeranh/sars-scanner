<?php
include '../config.php';

$sql = "SELECT * FROM sites WHERE active=1";

echo '<table>';
foreach ($pdo->query($sql) as $row){
    echo '<tr><td><a href="index.php?source='.$row['id'].'&type='.$row['type'].'">'.$row['title'].'</a></td><td>'.$row['link'].'</td></tr>';
}

if (isset($_GET['source'])){
    if (!isset($_GET['time'])){
        $sql = "SELECT * FROM scans WHERE site=? order by id desc";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1, $_GET['source']);
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        echo '<table>';
        foreach ($data as $row){
            echo '<tr><td><a href="index.php?source='.$_GET['source'].'&time='.$row['id'].'&type='.$_GET['type'].'">'.$row['time'].'</a></td></tr>';
        }
        echo '</table>';
    }else{
        if (isset($_GET['type'])){
            if ($_GET['type'] == 'csv'){
                $sql = "SELECT content FROM scans WHERE id=?";
                $stmt = $pdo->prepare($sql);
                $stmt->bindParam(1, $_GET['time']);
                $stmt->execute();
                $csv = $stmt->fetchAll(PDO::FETCH_ASSOC)[0]['content'];
                $test = str_getcsv($csv, ",", "\"", "\\");
                echo '<pre>';
                var_dump($test);
                echo '</pre>';
                echo '<table>';
                for ($i = 0; $i < count($test); $i++){
                    if ($i+1 % 12 == 0){
                        echo '</tr><tr>';
                    }

                    echo '<td>'.$test[$i].'</td>';
                }
                echo'<table>';
            }else{
                if ($_GET['type'] == 'html');
                $sql = "SELECT * FROM data WHERE scan=? order by amount desc";
                $stmt = $pdo->prepare($sql);
                $stmt->bindParam(1, $_GET['time']);
                $stmt->execute();
                $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
                echo '<table>';
                echo '<tr><th>id</th><th>Scan-No</th><th>Place</th><th>infected</th><th>dead</th><th>active</th><th>cured</th></tr>';
                foreach ($data as $row){
                    echo '<tr>';
                    echo '<td>'.$row['id'].'</td><td>'.$row['scan'].'</td><td><a href="laender.php?land='.$row['place'].'">'.$row['place'].'</a></td><td>'.$row['amount'].'</td><td>'.$row['deaths'].'</td><td>'.$row['active'].'</td><td>'.$row['cured'].'</td>';
                    echo '</tr>';
                }
                echo '</table>';
            }
        }else{
            $sql = "SELECT * FROM data WHERE scan=?";
            $stmt = $pdo->prepare($sql);
            $stmt->bindParam(1, $_GET['time']);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
            echo '<table>';
            foreach ($data as $row){
                echo '<tr><td>'.$row['place'].'</td><td>'.$row['amount'].'</td></tr>';
            }
            echo '</table>';
        }
    }
}