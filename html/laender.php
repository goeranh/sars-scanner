<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="refresh" content="60">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <title>My Chart.js Chart</title>
</head>
<body>

<?php
include "../config.php";

if(isset($_GET['land'])){
    $sql = "SELECT scans.id, place, amount, deaths, active, cured, time FROM `data` inner join scans on scans.id=data.scan WHERE place = ? group by amount order by time asc ";
    //$sql = "SELECT scans.id, place, amount, deaths, active, cured, time FROM `data` inner join scans on scans.id=data.scan WHERE place like ? group by amount order by time desc ";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(1, $_GET['land']);
    $stmt->execute();
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
    echo '<table style="width: 100%">';
    echo '<tr><th>id</th><th>Scan-No</th><th>infected</th><th>dead</th><th>active</th><th>cured</th><th>time</th></tr>';
    foreach ($data as $row){
        echo '<tr>';
        foreach ($row as $item) {
            echo '<td>'.$item.'</td>';
        }
        echo '</tr>';
    }
    echo '</table>';
}
?>

<div class="container"  style="height: 40vh; width: 80vh; position: relative">
    <canvas id="myChart"></canvas>
</div>

<script>
    let myChart = document.getElementById('myChart').getContext('2d');

        // Global Options
    Chart.defaults.global.defaultFontFamily = 'Lato';
    Chart.defaults.global.defaultFontSize = 18;
    Chart.defaults.global.defaultFontColor = '#777';

    let massPopChart = new Chart(myChart, {
        type:'bar', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
        data:{
            labels:[
                <?php
                    for ($i = 0 ; $i < count($data); $i++){
                        if ($i == 0){
                            echo "'".$data[$i]['time']."'";
                        }else{
                            echo ', \''.$data[$i]['time']."'";
                        }
                    }
                ?>
            ],
            datasets:[{
                label:'Anzahl',
                data:[
                    <?php
                    for ($i = 0 ; $i < count($data); $i++){
                        if ($i == 0){
                            echo "".$data[$i]['amount']."";
                        }else{
                            echo ', '.$data[$i]['amount']."";
                        }
                    }
                    ?>
                ],
                //backgroundColor:'green',
                backgroundColor:[
                    /*'rgba(255, 99, 132, 0.6)',
                    'rgba(54, 162, 235, 0.6)',
                    'rgba(255, 206, 86, 0.6)',
                    'rgba(75, 192, 192, 0.6)',
                    'rgba(153, 102, 255, 0.6)',
                    'rgba(255, 159, 64, 0.6)',
                    'rba(255, 99, 132, 0.6)'*/
                ],
                borderWidth:1,
                borderColor:'#777',
                hoverBorderWidth:3,
                hoverBorderColor:'#000'
            }]
        },
        options:{
            responsive: true,
            maintainAspectRatio: false,
            title:{
                display:true,
                text:'Anzahl der Infektionsfälle in <?php echo $_GET['land']?>',
                fontSize:25
            },
            legend:{
                display:true,
                position:'right',
                labels:{
                    fontColor:'#000'
                }
            },
            layout:{
                padding:{
                    left:50,
                    right:0,
                    bottom:0,
                    top:0
                }
            },
            tooltips:{
                enabled:true
            }
        }
    });
</script>
</body>
</html>