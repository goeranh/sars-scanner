#! /usr/bin/php
<?php
include "config.php";

$sql = "SELECT id, title, link FROM sites WHERE active=1";
$stmt = $pdo->query($sql);
$sites = $stmt->fetchAll(PDO::FETCH_ASSOC);
foreach ($sites as $site){
    echo 'fetching site: '.$site['title']."\n";
    $inhalt = file_get_contents($site['link']);
    $sql = "INSERT INTO `scans`(`site`, `content`) VALUES (?, ?)";
    $stmt = $pdo->prepare($sql);
    $inhalt = str_replace("\n", "<!---break--->", $inhalt);
    $stmt->bindParam(1, $site['id']);
    $stmt->bindParam(2, $inhalt);
    $stmt->execute();
}