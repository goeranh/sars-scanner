#! /usr/bin/php
<?php
include "config.php";

$sql = "SELECT * FROM scans WHERE site=2 and parsed=0";
//$sql = "SELECT * FROM scans WHERE id=(SELECT MAX(id) FROM scans WHERE site=2)";
$stmt = $pdo->query($sql);
$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
foreach ($data as $scan){
    $document = new DOMDocument();
    $document->loadHTML($scan['content']);
    $table = $document->getElementById("main_table_countries_div");
    $table = $table->childNodes[1];
    $data = $table->childNodes[1];
    $parts = $data->childNodes;
    $linetotal = array();
    $linetotal[] = $table->childNodes[2]->childNodes[0]->childNodes[0]->textContent;
    $headline = $table->childNodes[0]->childNodes[0]->childNodes;

    $deathsCol = 0;
    $recoveredCol = 12;
    $activeCol = 16;
    for ($j = 0; $j < $headline->length; $j++){
        //var_dump($j);
        //var_dump($headline[$j]);
        if ($headline[$j]->textContent == 'TotalDeaths'){
            $deathsCol = $j;
        }
        if ($headline[$j] == 'TotalRecovered'){
            $recoveredCol = $j;
        }
        if ($headline[$j] == 'ActiveCases'){
            var_dump("test");
            $activeCol = $j;
        }
    }

    echo $deathsCol . ' ' . $recoveredCol . ' ' . $activeCol;

    $linetotal[] = intval(str_replace(",", "", $table->childNodes[2]->childNodes[0]->childNodes[2]->textContent));
    $document = array();
    $document[] = $linetotal;
    for ($i = 0; $i < $parts->length; $i++) {
        $country = array();
        $p2 = $parts[$i]->childNodes;
        $country[] = trim($p2[0]->textContent);
        /*for ($j=0; $j < $p2->length; $j++){
            var_dump($p2[$j]->textContent);
        }*/
        //$deaths = (strpos($p2[6]->textContent, "+") !== false?$p2[8]->textContent:$p2[6]->textContent);
        //$active = (strpos($p2[6]->textContent, "+") !== false?$p2[16]->textContent:$p2[10]->textContent);
        $deaths = $p2[$deathsCol]->textContent;
        $recovered = $p2[$recoveredCol]->textContent;
        $active = $p2[$activeCol]->textContent;
        //var_dump($activeCol);
        //var_dump($p2);
        $country[] = intval(str_replace(",", "", $p2[2]->textContent));
        $country[] =intval(str_replace(",", "", $deaths));
        $country[] =intval(str_replace(",", "", $active));
        $country[] =intval(str_replace(",", "", $recovered));
        $document[] = $country;
    }

    $scanid = $scan['id'];

    foreach ($document as $entry){
        $sql = "INSERT INTO `data`(`scan`, `place`, `amount`, deaths, active, cured) VALUES (?, ?, ?, ?, ?, ?)";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(1, $scanid);
        $stmt->bindParam(2, $entry[0]);
        $stmt->bindParam(3, $entry[1]);
        $stmt->bindParam(4, $entry[2]);
        $stmt->bindParam(5, $entry[3]);
        $stmt->bindParam(6, $entry[4]);
        $stmt->execute();
        if($stmt->errorInfo()[0] != '00000' and $stmt->errorInfo()[0] != '23000' and $stmt->errorInfo()[0] != '22007')
            var_dump($stmt->errorInfo());
    }

    $sql = "UPDATE `scans` SET `parsed`=1 WHERE id=?";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(1, $scanid);
    $stmt->execute();
}