<?php
include 'config.php';

$sql = "SELECT id FROM scans";
$stmt1 = $pdo->query($sql);
$stmt2 = $pdoSlave->query($sql);
$data1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
$data2 = $stmt2->fetchAll(PDO::FETCH_ASSOC);

$idsMaster = array();
$idsSlave = array();
foreach ($data1 as $item) {
    $idsMaster[] = $item['id'];
}

foreach ($data2 as $item){
    $idsSlave[] = $item['id'];
}

$idsToCopy = array();
foreach ($idsMaster as $item) {
    if (!in_array($item, $idsSlave)){
        $idsToCopy[] = $item;
    }
}

foreach ($idsToCopy as $item) {
    $sql = "SELECT * FROM scans WHERE id=?";
    $stmt1 = $pdo->prepare($sql);
    $stmt1->bindParam(1, $item);
    if ($stmt1->execute()){
        $data = $stmt1->fetchAll(PDO::FETCH_ASSOC)[0];
        $sqlPrepare = "INSERT INTO `scans`(`id`, `site`, `time`, `content`, `parsed`) VALUES (?,?,?,?,?)";
        $stmt2 = $pdoSlave->prepare($sqlPrepare);
        $stmt2->bindParam(1, $data['id']);
        $stmt2->bindParam(2, $data['site']);
        $stmt2->bindParam(3, $data['time']);
        $stmt2->bindParam(4, $data['content']);
        $stmt2->bindParam(5, $data['parsed']);
        if ($stmt2->execute()){
            echo "data written\n";
        }else{
            var_dump($stmt2->errorInfo());
        }
    }else{
        var_dump($stmt1->errorInfo());
    }
}