<?php
include 'config.php';

$sql = "SELECT * FROM scans where site=4 and parsed=0";
$stmt = $pdo->query($sql);
$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
foreach ($data as $row){
    $teile = explode("<!---break--->", $row['content']);
    foreach ($teile as $teil){
        $laender = str_getcsv($teil);
        if (count($laender)>1){
            if ($laender[0] != 'parent'){
                $data = array();
                $data['country'] = $laender[0];
                $data['state'] = $laender[1];
                $data['infected'] = $laender[5];
                $data['cured'] = $laender[6];
                $data['deaths'] = $laender[7];
                $sql = "INSERT INTO `data`(`scan`, `place`, `amount`, `deaths`, `cured`) VALUES (?,?,?,?,?)";
                $stmt =$pdo->prepare($sql);
                $stmt->bindParam(1, $row['id']);
                $place = $data['country'].'.'.$data['state'];
                $stmt->bindParam(2, $place);
                $stmt->bindParam(3, $data['infected']);
                $stmt->bindParam(4, $data['deaths']);
                $stmt->bindParam(5, $data['cured']);
                if(!$stmt->execute()){
                    if ($stmt->errorInfo()[0] != '23000'){
                        var_dump($stmt->errorInfo());
                    }
                }
            }
        }
    }
    $sql = "UPDATE `scans` SET `parsed`=true WHERE id=?";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(1, $row['id']);
    if (!$stmt->execute()){
        var_dump($stmt->errorInfo());
    }
}